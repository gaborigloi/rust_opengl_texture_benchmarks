// Modified parts from the file
// https://github.com/bjz/gl-rs/blob/c68ff55946e41f7d01c33658d47e9716682c10e2/gl/examples/basic.rs

// Copyright 2015 Brendan Zabarauskas and the gl-rs developers
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate criterion;
extern crate glium;
extern crate gl;

use criterion::{Bencher, Criterion};

fn main() {
    use glium::DisplayBuild;
    use glium::GlObject;

    // Initializing once outside the benchmark closures produced the same results (with 1-6% difference)
    fn init() -> (glium::Display, glium::Texture2d) {

        let display = glium::glutin::WindowBuilder::new()
                          .with_visibility(false)
                          .build_glium()
                          .unwrap();
        {
            let window = display.get_window().unwrap();

            // It is essential to make the context current before calling `gl::load_with`.
            unsafe { window.make_current() }.unwrap();

            // Load the OpenGL function pointers
            // TODO: `as *const _` will not be needed once glutin is updated to the latest gl version
            gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
        }

        let texture = glium::Texture2d::empty(&display, 64, 64).unwrap();

        (display, texture)
    }

    // This is more expensive than rebinding the texture:
    fn gl_get_info(b: &mut Bencher) {
        let (display, _) = init();
        b.iter_with_large_drop(|| {
            unsafe {
                display.exec_in_context(|| {
                    let mut a: gl::types::GLint = 0;
                    gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut a as *mut _);
                });
            }
        });
    };

    fn gl_bind_texture(b: &mut Bencher) {
        let (display, texture) = init();
        b.iter_with_large_drop(|| {
            unsafe {
                display.exec_in_context(|| {
                    // the OpenGL state should be restored
                    gl::BindTexture(gl::TEXTURE_2D, texture.get_id());
                });
            }
        });
    };

    fn gl_bind_texture_and_restore_state(b: &mut Bencher) {
        let (display, texture) = init();
        b.iter_with_large_drop(|| {
            unsafe {
                display.exec_in_context(|| {
                    let mut orig: gl::types::GLint = 0;
                    gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut orig as *mut _);
                    gl::BindTexture(gl::TEXTURE_2D, texture.get_id());
                    // restore the OpenGL state:
                    gl::BindTexture(gl::TEXTURE_2D, orig as u32);
                });
            }
        });
    };

    Criterion::default()
        .bench_function("gl_get_info", gl_get_info)
        .bench_function("gl_bind_texture", gl_bind_texture)
        .bench_function("gl_bind_texture_and_restore_state",
                        gl_bind_texture_and_restore_state);
}
