# Measuring OpenGL shader performance:

* <https://stackoverflow.com/questions/28175631/how-to-measure-time-performance-of-a-compute-shader>
* <https://www.opengl.org/discussion_boards/showthread.php/168120-Measuring-GLSL-shader-program-running-time?p=1185724&viewfull=1#post1185724>
* <https://www.opengl.org/wiki/Performance>